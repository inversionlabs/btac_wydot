#!/usr/bin/env python

# Script to analyze slide paths affecting roadways managed by WYDOT

# This script is a collection of base code and snippets, and is very partial.
# See Jupyter Notebook for finished code and plots.

# Patrick Wright, Inversion Labs
# Job: BTAC
# started August 20, 2016

import numpy as np
import pandas as pd
from IPython import embed
import matplotlib.pyplot as plt

# --------------------------------------------------------------------------
def histoplot_crown(data,titlestring, xlabel):
	num_bins=np.arange(-0.5, 100.5, 1)
	fig10 = plt.figure(figsize=(7,5))
	#fig10 = plt.figure(figsize=(9 * 1.618, 9))
	ax = fig10.add_subplot(111)
	n, bins, patches = plt.hist(data, num_bins, facecolor='blue', alpha = 0.5)
	#plt.axvline(x=np.mean(data), ymin=0, ymax=np.max(data), linewidth=1.5, color='r')
	#plt.axvline(x=np.median(data), ymin=0, ymax=180, linewidth=1.5, color='k')
	ax.set_xlim(0,100)
  #xmin, xmax = ax.get_xlim()
	#ymin, ymax = ax.get_ylim()
	#plt.text(xmax-0.35*xmax,ymax-0.08*ymax,'mean = %.1f' %(settle_storm_mid_mean * 2.54), color='r', fontsize=22)
	#plt.text(xmax-0.35*xmax,ymax-0.13*ymax,'median = %.1f' %(settle_storm_mid_median * 2.54), fontsize=22)
	ax.set_title(titlestring, fontsize=18)
	ax.set_xlabel(xlabel, fontsize=18)
	ax.set_ylabel('Count', fontsize=18)
	plt.xticks(np.arange(0, 102, 6))
	plt.tick_params(axis='both', which='major', labelsize=18)
	#plt.text(-5.5,268,'A.',horizontalalignment='center',verticalalignment='center', fontsize=32)
	fig10.tight_layout(pad=1.5)
	#plt.savefig('%s.png' %titlestring,dpi=450)

def histoplot_size(data,titlestring, xlabel):
	num_bins=np.arange(-0.5, 6, 1)
	fig10 = plt.figure(figsize=(7,5))
	ax = fig10.add_subplot(111)
	n, bins, patches = plt.hist(data, num_bins, facecolor='blue', alpha = 0.5)
	#plt.axvline(x=np.mean(data), ymin=0, ymax=np.max(data), linewidth=1.5, color='r')
	#plt.axvline(x=np.median(data), ymin=0, ymax=180, linewidth=1.5, color='k')
	ax.set_xlim(0,6)
  #xmin, xmax = ax.get_xlim()
	#ymin, ymax = ax.get_ylim()
	#plt.text(xmax-0.35*xmax,ymax-0.08*ymax,'mean = %.1f' %(settle_storm_mid_mean * 2.54), color='r', fontsize=22)
	#plt.text(xmax-0.35*xmax,ymax-0.13*ymax,'median = %.1f' %(settle_storm_mid_median * 2.54), fontsize=22)
	ax.set_title(titlestring, fontsize=18)
	ax.set_xlabel(xlabel, fontsize=18)
	ax.set_ylabel('Count', fontsize=18)
	plt.xticks(np.arange(0, 6, 1))
	plt.tick_params(axis='both', which='major', labelsize=18)
	#plt.text(-5.5,268,'A.',horizontalalignment='center',verticalalignment='center', fontsize=32)
	fig10.tight_layout(pad=1.5)
	#plt.savefig('%s.png' %titlestring,dpi=450)
	
def histoplot_trigger(data,titlestring, xlabel):
	num_bins=np.arange(-0.5, 100, 1)
	fig10 = plt.figure(figsize=(7,5))
	ax = fig10.add_subplot(111)
	n, bins, patches = plt.hist(data, num_bins, facecolor='blue', alpha = 0.5)
	#plt.axvline(x=np.mean(data), ymin=0, ymax=np.max(data), linewidth=1.5, color='r')
	#plt.axvline(x=np.median(data), ymin=0, ymax=180, linewidth=1.5, color='k')
	ax.set_xlim(0,100)
  #xmin, xmax = ax.get_xlim()
	#ymin, ymax = ax.get_ylim()
	#plt.text(xmax-0.35*xmax,ymax-0.08*ymax,'mean = %.1f' %(settle_storm_mid_mean * 2.54), color='r', fontsize=22)
	#plt.text(xmax-0.35*xmax,ymax-0.13*ymax,'median = %.1f' %(settle_storm_mid_median * 2.54), fontsize=22)
	ax.set_title(titlestring, fontsize=18)
	ax.set_xlabel(xlabel, fontsize=18)
	ax.set_ylabel('Count', fontsize=18)
	plt.xticks(np.arange(0, 100, 1))
	plt.tick_params(axis='both', which='major', labelsize=18)
	#plt.text(-5.5,268,'A.',horizontalalignment='center',verticalalignment='center', fontsize=32)
	fig10.tight_layout(pad=1.5)
	#plt.savefig('%s.png' %titlestring,dpi=450)

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

#--------------------------------------------------------------------------------------
# IMPORT EVENT DATA
#--------------------------------------------------------------------------------------

print ''
print "Processing event datafiles..."

# Define directory paths:
fpath_4='data/PatrickHistoricalAvalancheTable.csv' # event table

# Read in data:
data4 = pd.read_csv(fpath_4, sep=',')

# Convert datetime strings to a Pandas datetime object
timestamp4 = data4['fldDate']
dt4 = pd.to_datetime(timestamp4, infer_datetime_format=True)

# set index of dataframes to datetime
data4_dt = data4.set_index(dt4)
data4_dt.sort_index(inplace=True)

# get rid of redundant date column:
data4_dt.drop('fldDate', axis=1, inplace=True)

#--------------------------------------------------------------------------------------
path = data4_dt.PathName
Type = data4_dt.fldType
trigger = data4_dt.Trigger
size_rel = data4_dt.Size
size_des = data4_dt.DestructiveSize
crown_depth = data4_dt.Depth
notes = data4_dt.Notes
notes_public = data4_dt.PublicNotes
#--------------------------------------------------------------------------------------

# GLORY BOWL

d = {'pathname':path, 'type':Type, 'trigger':trigger, 'R size':size_rel, 'D size':size_des, 
     'crown depth':crown_depth, 'notes':notes, 'public notes':notes_public}

full_table = pd.DataFrame(data=d, index=data4_dt.index)

Glory_table = full_table[full_table.pathname.str.contains('glory', case=False)==True]

glory_drop_list = ['BowlnorthofGlory','n. of glory bowl','gloryBacksideWestAspect','glory/little tucks','BtwGlory&Shovelslide','glory great white hump',
                   'horseshoe bowl, n of glory','Glory Twin Slides','chicken scratch- mt glory','GloryTwinSlide','West side of Glory','Glory Chicken Scratch',
                   'Mt. Glory Twin Slide','Glory Chicken Scratch Couloir','Mt Glory Skiers left of First Turn','Mt Glory Chicken Scratch']

def drop_false(dataframe,false_list):
  for entry in false_list:
    dataframe = dataframe[dataframe.pathname != entry]
  return dataframe

Glory_table = drop_false(Glory_table,glory_drop_list)

#Glory_table.to_csv("Glory_table.csv",columns=['pathname','type','trigger','R size','D size','crown depth','notes','public notes'],index=True)

#--------------------------------------------------------------------------------------

# TWIN SLIDES

Twin_table = full_table[full_table.pathname.str.contains('twin', case=False)==True]

#Twin_table.to_csv("Twin_table.csv",columns=['pathname','type','trigger','R size','D size','crown depth','notes','public notes'],index=True)


#--------------------------------------------------------------------------------------

# SHOVEL SLIDE

Shovel_table = full_table[full_table.pathname.str.contains('shovel', case=False)==True]

shovel_drop_list = ['AngleMtnshovelSlideESEAspect']

Shovel_table = drop_false(Shovel_table,shovel_drop_list)
#Shovel_table.to_csv("Shovel_table.csv",columns=['pathname','type','trigger','R size','D size','crown depth','notes','public notes'],index=True)

#--------------------------------------------------------------------------------------

# MILEPOST 151

mp151_table = full_table[full_table.pathname.str.contains('151', case=False)==True]

mp151_drop_list = ['s. park gravel pit (HWY 151)']

mp151_table = drop_false(mp151_table,mp151_drop_list)
#mp151_table.to_csv("mp151_table.csv",columns=['pathname','type','trigger','R size','D size','crown depth','notes','public notes'],index=True)

#--------------------------------------------------------------------------------------

# COW, CALF, BULL

cow_table = full_table[full_table.pathname.str.contains('cow', case=False)==True]
calf_table = full_table[full_table.pathname.str.contains('calf', case=False)==True]
bull_table = full_table[full_table.pathname.str.contains('Bull of the Woods', case=False)==True] #only one unique record

cow_drop_list = ['cow slide in yellowstone','Cowboys and Indians','Cowboys&Indians']
cow_table = drop_false(cow_table,cow_drop_list)

#cow_table.to_csv("cow_table.csv",columns=['pathname','type','trigger','R size','D size','crown depth','notes','public notes'],index=True)

#calf_table.to_csv("calf_table.csv",columns=['pathname','type','trigger','R size','D size','crown depth','notes','public notes'],index=True)

#bull_table.to_csv("bull_table.csv",columns=['pathname','type','trigger','R size','D size','crown depth','notes','public notes'],index=True)

#--------------------------------------------------------------------------------------

embed()

##--------------------------------------------------------------------------------------
## PLOTTING
##--------------------------------------------------------------------------------------

#fig, ax = plt.subplots(figsize=(15,5))
#ax.bar(glory_path_list_grouped_year.index, glory_path_list_grouped_year, align='center', color='b', alpha=0.5)
#plt.xticks(np.arange(1974, 2016, 1))
#plt.tick_params(axis='both', which='major', labelsize=16)
#plt.ylabel('Count', fontsize=16)
#plt.xticks(fontsize = 16, rotation='90')
#plt.yticks(fontsize = 16)
#plt.title('Glory Recorded Events (1974-2016)', fontsize=16)
#fig.tight_layout(pad=1.5)

#fig, ax = plt.subplots(figsize=(15,5))
#ax.bar(twin_path_list_grouped_year.index, twin_path_list_grouped_year, align='center', color='b', alpha=0.5)
#plt.xticks(np.arange(1974, 2016, 1))
#plt.tick_params(axis='both', which='major', labelsize=16)
#plt.ylabel('Count', fontsize=16)
#plt.xticks(fontsize = 16, rotation='90')
#plt.yticks(fontsize = 16)
#plt.title('Twin Slides Recorded Events (1974-2016)', fontsize=16)
#fig.tight_layout(pad=1.5)

##--------------------------------------------------------------------------------------

#fig, ax = plt.subplots(figsize=(15,5))
#ax.bar(glory_twin_trigger_art.index-0.3, glory_twin_trigger_art, width=0.3, align='center', color='b', alpha=0.5, label='Artillery')
#ax.bar(glory_twin_trigger_gaz.index, glory_twin_trigger_gaz, width=0.3, align='center', color='r', alpha=0.5, label='GAZEX')
#ax.bar(glory_twin_trigger_hand.index+0.3, glory_twin_trigger_hand, width=0.3, align='center', color='g', alpha=0.5, label='Hand Charge')
#plt.tick_params(axis='both', which='major', labelsize=16)
#plt.ylabel('Count', fontsize=16)
#plt.xticks(fontsize = 16, rotation='90')
#plt.yticks(fontsize = 16)
#plt.title('Glory/Twin combined: explosive triggers', fontsize=16)
#plt.xticks(np.arange(1974, 2016, 1))
#plt.legend()
#fig.tight_layout(pad=1.5)

#fig, axs = plt.subplots(1,1)
#glory_twin_trigger_counts.plot(kind='bar', alpha=0.5)
#plt.xlabel('Trigger code', fontsize=14)
#plt.ylabel('Count', fontsize=14)
#plt.xticks(fontsize = 16)
#plt.yticks(fontsize = 14)
#plt.title('Glory/Twin combined: triggers by type (1974-2016)', fontsize=16)

#t1 = axs.text(0.58,0.92, '  N: Natural', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t2 = axs.text(0.58,0.88, 'AX: GAZEX', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t3 = axs.text(0.58,0.84, 'AA: Artillery', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t4 = axs.text(0.58,0.80, 'AS: Skier', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t5 = axs.text(0.58,0.76, 'AR: Snowboarder', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t6 = axs.text(0.58,0.72, 'AE: Hand charge', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t7 = axs.text(0.58,0.68, 'AF: Foot penetration', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t8 = axs.text(0.58,0.64, 'AO: Unclassified artificial', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#t9 = axs.text(0.58,0.60, 'AD: ?', ha='left', color='k', fontsize=14, transform=axs.transAxes)
#fig.canvas.draw()

##--------------------------------------------------------------------------------------

#histoplot_crown(glory_crown,'Glory Crown Depth (1974-2016)', 'crown depth (in)')
#histoplot_crown(twin_crown,'Twin Crown Depth (1974-2016)', 'crown depth (in)')

#histoplot_size(glory_size,'Glory Size (1974-2016)', 'relative size')
#histoplot_size(twin_size,'Twin Size (1974-2016)', 'relative size')

##plt.show()


  